import numpy as np
import theano
import theano.tensor as T
#import matplotlib.pyplot as plt

from sklearn.utils import shuffle
from theano.tensor.nnet import conv2d
from theano.tensor.signal import downsample

from util import error_rate, init_weight_and_bias, init_filter # init_filter creates 4D filter.
from ann_theano import HiddenLayer
import pandas as pd
import pickle


class ConvPoolLayer(object):
    def __init__(self, mi, mo, fw=5, fh=5, poolsz=(2, 2)): # input feature maps, output feature maps, filter width, height, pool size
        # mi = input feature map size
        # mo = output feature map size
        self.sz = (mo, mi, fw, fh) # size tuple, output, input, width, height
        W0 = init_filter(self.sz, poolsz) # init the filter
        self.W = theano.shared(W0) # init the theano shared variable
        b0 = np.zeros(mo, dtype=np.float32) # set bias to zeros
        self.b = theano.shared(b0) # create a theano shared
        self.poolsz = poolsz # keep the pool size 
        self.params = [self.W, self.b] # keep the params for the gradients

    def forward(self, X):
        conv_out = conv2d(input=X, filters=self.W) # do the convolution
        pooled_out = downsample.max_pool_2d( # max pooling
            input=conv_out,
            ds=self.poolsz,
            ignore_border=True
        )
        return T.tanh(pooled_out + self.b.dimshuffle('x', 0, 'x', 'x')) # using tanh as the activation function.. broadcasting the bias


class CNN(object):
    def __init__(self, convpool_layer_sizes, hidden_layer_sizes): # take in convpool layer sizes and hidden layer sizes
        self.convpool_layer_sizes = convpool_layer_sizes
        self.hidden_layer_sizes = hidden_layer_sizes

    def fit(self, X, Y, lr=10e-5, mu=0.99, reg=10e-7, decay=0.99999, eps=10e-3, batch_sz=30, epochs=30, show_fig=True): # b=30epochs was 100
    	# learning rate, momentum, regularization, decay, epsilon for rmsprop, batch size, epochs, show fig
        lr = np.float32(lr) # for theano to work, cast everything to float32
        mu = np.float32(mu)
        reg = np.float32(reg)
        decay = np.float32(decay)
        eps = np.float32(eps)

        # make a validation set
        X, Y = shuffle(X, Y) # shuffle the data
        X = X.astype(np.float32) # ensure X is float 32
        Y = Y.astype(np.int32) # y is int 32

        Xvalid, Yvalid = X[-1000:], Y[-1000:] # 1000 samples for the validation set
        X, Y = X[:-1000], Y[:-1000] # X and Y as the rest

        # initialize convpool layers
        N, c, d, d = X.shape # N x color x width x height
        mi = c # 1st input feature map is num colors
        outw = d # keep track of output layer sizes
        outh = d
        self.convpool_layers = []
        for mo, fw, fh in self.convpool_layer_sizes: # loop through the triples
            layer = ConvPoolLayer(mi, mo, fw, fh) # create a layer
            self.convpool_layers.append(layer) # append this to convpool layers
            outw = (outw - fw + 1) / 2 # calculate next outw and outh.. aftern convolution is this equation
            outh = (outh - fh + 1) / 2
            mi = mo # set the next input layer to this output

        # initialize mlp layers
        K = len(set(Y)) # all unique values in Y
        self.hidden_layers = [] # list for hidden layers
        M1 = self.convpool_layer_sizes[-1][0]*outw*outh # size must be same as output of last convpool layer, last element in convpool, num filter maps * outw * outh.. tht's going to be input to first fully connected hidden layer
        count = 0 # for keeping the IDs
        for M2 in self.hidden_layer_sizes: 
            h = HiddenLayer(M1, M2, count) # create hidden layer
            self.hidden_layers.append(h) # append to list
            M1 = M2 # output becomes the next input
            count += 1 # increment the count

        # logistic regression layer
        W, b = init_weight_and_bias(M1, K) # size M1 x K
        self.W = theano.shared(W, 'W_logreg') # create Theano shareds
        self.b = theano.shared(b, 'b_logreg')

        # collect params for later use
        self.params = [self.W, self.b] # keep the params for doing gradient descent later
        for c in self.convpool_layers: # go through convpool layers
            self.params += c.params
        for h in self.hidden_layers: # go through the hidden layers, add those, all params in one list
            self.params += h.params

        # for momentum.. initialized as zeros.. make sure float32
        dparams = [theano.shared(np.zeros(p.get_value().shape, dtype=np.float32)) for p in self.params]

        # for rmsprop.. init as zeros, float 32
        cache = [theano.shared(np.zeros(p.get_value().shape, dtype=np.float32)) for p in self.params]

        # set up theano functions and variables 
        thX = T.tensor4('X', dtype='float32') # input is 4D tensor
        thY = T.ivector('Y') # Targets remain the same
        pY = self.forward(thX) 

        rcost = reg*T.sum([(p*p).sum() for p in self.params]) # regularization cost
        cost = -T.mean(T.log(pY[T.arange(thY.shape[0]), thY])) + rcost # get the cost
        prediction = self.predict(thX) 

        cost_predict_op = theano.function(inputs=[thX, thY], outputs=[cost, prediction]) # get the cost in prediction, without updates, for calc cost and predict on validation set

        # for rms prop and momentum
        # updates = [
        #     (c, decay*c + (np.float32(1)-decay)*T.grad(cost, p)*T.grad(cost, p)) for p, c in zip(self.params, cache)
        # ] + [
        #     (p, p + mu*dp - lr*T.grad(cost, p)/T.sqrt(c + eps)) for p, c, dp in zip(self.params, cache, dparams)
        # ] + [
        #     (dp, mu*dp - lr*T.grad(cost, p)/T.sqrt(c + eps)) for p, c, dp in zip(self.params, cache, dparams)
        # ]

        # momentum only
        updates = [
            (p, p + mu*dp - lr*T.grad(cost, p)) for p, dp in zip(self.params, dparams) 
        ] + [
            (dp, mu*dp - lr*T.grad(cost, p)) for p, dp in zip(self.params, dparams)
        ]

        train_op = theano.function( # define the train op for updates, no outputs for this function
            inputs=[thX, thY],
            updates=updates 
        )

        n_batches = N / batch_sz # get number of batches
        costs = [] # keep track of costs for graph
        for i in xrange(epochs): # loop through epochs
            X, Y = shuffle(X, Y)
            for j in xrange(n_batches):
                Xbatch = X[j*batch_sz:(j*batch_sz+batch_sz)]
                Ybatch = Y[j*batch_sz:(j*batch_sz+batch_sz)]

                train_op(Xbatch, Ybatch) 

                if j % 20 == 0: # every 20 steps, do cost and predict on the validation set
                    c, p = cost_predict_op(Xvalid, Yvalid)
                    costs.append(c)
                    e = error_rate(Yvalid, p)
                    print "i:", i, "j:", j, "nb:", n_batches, "cost:", c, "error rate:", e

        #if show_fig:
            #plt.plot(costs)
            #plt.show()

    def forward(self, X):
        Z = X
        for c in self.convpool_layers: # loop through the convpool layers
            Z = c.forward(Z)
        Z = Z.flatten(ndim=2) # flatten Z for input to mlp
        for h in self.hidden_layers: # loop through the hidden layers
            Z = h.forward(Z)
        return T.nnet.softmax(Z.dot(self.W) + self.b) # do the softmax on the final layer

    def predict(self, X):
        pY = self.forward(X)
        return T.argmax(pY, axis=1) # returns argmax P_Y | X
        
    def save_state(self, filePath): ### use this to save the Weights and biases

		##### Previous

#		ParamFile = filePath

#		np.save(ParamFile, self.params)
#		print "Classifier saved to: " + filePath

		### to convert from CUDA Narray from the GPU.. works.. but breaks other parts of program
		#ParamFile=filePath
		#layerParams=[]
		#for layer in self.params:
			#layerParams.append(np.asarray(layer))
		
		#np.save(ParamFile, layerParams)
		#print "Classifier saved to: " + filePath
		
		######
		###Convert all elements to Numpy Array and save separately before saving to dictionary object
		cnn_dict = {}
		ParamFile = filePath+"cnn_dict.npy"
		# save the logistic regression layers 		
		W_logreg = np.asarray(self.W.eval())
		b_logreg = np.asarray(self.b.eval())
		cnn_dict['W_logreg'] = W_logreg
		cnn_dict['b_logreg'] = b_logreg
		
		####save the convolutional layers in a list of Weight, bias, poolsize

		name='convpool'
		count=0
		for c in self.convpool_layers: # go through convpool layers.. add W, then b, then pool size
			c_W = np.asarray(c.W.eval())
			c_b = np.asarray(c.b.eval())
			c_poolsz = c.poolsz
			c_sz = c.sz
			cnn_dict[name+str(count)]=[c_W, c_b, c_poolsz, c_sz]
			count+=1
		
			
		###save the hidden layers
		for h in self.hidden_layers:
			h_W = np.asarray(h.W.eval())
			h_b = np.asarray(h.b.eval())
			h_M1 = h.M1
			h_M2=h.M2
			h_id=h.id
			cnn_dict['h'+str(h.id)]=[h_M1, h_M2, h_W, h_b] # save input dimensions, output dim, Weights, and bias per layer
			
			
				

		#np.save(ParamFile, cnn_dict)
		pickle.dump( cnn_dict, open( filePath+"cnn_dict.p", "wb" ) )
		print "CNN Dictionary saved to: " + filePath
		
	
        
def get_normalized_data():
	# get raw pixels data, then mean normalized and feature scaled by the pixel data
	print "Reading in and transforming data..."
	df = pd.read_csv('../large_files/train.csv')
	data = df.as_matrix().astype(np.float32)
	np.random.shuffle(data)
	X = data[:, 1:]
	mu = X.mean(axis=0)
	std = X.std(axis=0)
	np.place(std, std == 0, 1)
	X = (X - mu) / std # normalize the data
	Y = data[:, 0]
	return X, Y

def getImageData():
	X, Y = get_normalized_data()
	N, D = X.shape
	d = int(np.sqrt(D))
	X = X.reshape(N, 1, d, d) # N samples, 1 color channel, width d, and height d (48 x 48 in this case)
	return X, Y


def main():
	print "Loading the Data..."
	X, Y = getImageData() # don't want the flat data... need the data as images
	print "Data Loaded. Training.."
	model = CNN(
		convpool_layer_sizes=[(20, 5, 5), (20, 5, 5)], # 2 convolution layers, number of featue maps, width and height
		hidden_layer_sizes=[500, 300], # 500 x 300 sizes
	)
	model.fit(X, Y)
	filePath="./Classifier_Save/"
	model.save_state(filePath)

if __name__ == '__main__':
    main()
