This file contains a Convolutional Neural Network written in Theano for classifying hand written digits for the MNIST data set. This code achieves accuracy of 98% on the Kaggle Corpus.
